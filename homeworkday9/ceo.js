const fs = require('fs');
const { Employee } = require('./employee.js');
const { Programmer } = require('./programmer.js');
const { OfficeCleaner } = require('./officecleaner.js');

class CEO extends Employee {
    constructor(id, firstname, lastname, salary, role, dressCode) {
        super(id, firstname, lastname, salary, role);
        this.dressCode = dressCode;

        //this.employeesRaw = JSON.Parse()        
    }
    getSalary() {  // simulate public method
        return super.getSalary() * 2;
    };
    Work(employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
        if (employee.setSalary(newSalary)) { // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary

            console.log(employee.firstname + ""+"'s salary is less than before!!");
        } else {         
        }
        console.log(employee.firstname + ""+"'s salary has been set to"+ newSalary);
    }
    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);

    };
    
    _fire(employee) {  //Somsri has been fired! Dress with :tshirt
        this.dressCode = 'tshirt';
        console.log(employee.firstname + "has been fired! " + " Dress with :" + this.dressCode);
    }
    _hire(employee) {  //Somsri has been hired back! Dress with :tshirt
        this.dressCode = 'tshirt';
        console.log(employee.firstname + "has been hired back! " + " Dress with :" + this.dressCode);
    }
    _seminar() { // He is going to seminar Dress with :suit
        this.dressCode = 'suit';
        console.log( "He is going to seminar" + " Dress with :" + this.dressCode);
    }


    talk(message) {
        console.log(message);
    }

    reportRobot(self, robotMessage) {
        self.talk(robotMessage);
    }


    async readFile() {
        let self = this;
        try {
            let employees = await new Promise (function (resolve, reject) {
                fs.readFile('employee9.json','utf8',function(err,data){
                    let employees =[]
                    if (err)
                        reject(err)
                    else {
                        self.employeesRaw = JSON.parse(data)
                        for(let i = 0; i < self.employeesRaw.length;i++){
                            if(self.employeesRaw[i].role == 'CEO'){
                                employees[i] = new CEO (
                                    self.employeesRaw[i].id,  
                                    self.employeesRaw[i].firstname,
                                    self.employeesRaw[i].lastname,
                                    self.employeesRaw[i].salary,
                                    self.employeesRaw[i].role,
                                    self.employeesRaw[i].dressCode)
                            }
                            else if (self.employeesRaw[i].role == 'OfficeCleaner'){
                                employees[i] = new OfficeCleaner ( 
                                        self.employeesRaw[i].id,
                                        self.employeesRaw[i].firstname,
                                        self.employeesRaw[i].lastname,
                                        self.employeesRaw[i].salary,
                                        self.employeesRaw[i].role,
                                        self.employeesRaw[i].dressCode)                                    
                            }
                            else if(self.employeesRaw[i].role == 'Programmer'){                               
                                employees[i] = new Programmer (
                                    self.employeesRaw[i].id,
                                    self.employeesRaw[i].firstname,
                                    self.employeesRaw[i].lastname,
                                    self.employeesRaw[i].salary,
                                    self.employeesRaw[i].role,
                                    self.employeesRaw[i].type)  
                            }  
                        }
                        self.employees = employees
                        //console.log(self.employees)
                        resolve(self.employees)
                        }  
                                                    
                    })
                })  
                //console.log(employees)
                return employees;                  
            }
        catch (err) {
            console.error(err)
        }

    }
}
     
exports.CEO = CEO;