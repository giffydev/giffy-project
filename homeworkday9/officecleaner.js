const { Employee } = require('./employee.js')

class OfficeCleaner extends Employee {
    constructor(id,firstname, lastname, salary, role,dressCode) {
        super(id,firstname, lastname, salary,role)
        this.dressCode = dressCode;
    }
    

    Work() {
        this._Clean();
        this._KillCoachroach();
        this._DecorateRoom();
        this._WelcomeGuest();

    }


    _Clean() {
        console.log('Clean');
        
    }
        
    _KillCoachroach() {
       console.log('KillCoachroach')
    }       
    
    _DecorateRoom() {
        console.log('DecorateRoom')
        
    }
    
    _WelcomeGuest() {
        console.log('WelcomeGuest')
    }


}

exports.OfficeCleaner = OfficeCleaner;