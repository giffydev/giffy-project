const {Employee} = require('./employee.js');
class Programmer extends Employee {
    constructor(firstname, lastname, salary, id, type = 'frontend') {
        super(firstname, lastname, salary)
        this._id = id
        this.type = type = 'frontend';
    }
    work() {
        this.CreateWebsite()
        this.FixPC()
        this.InstallWindows()
        }

    CreateWebsite() {
        console.log('Website Complete')
    }
    
    FixPC(){
        console.log('Fix PC Complete')
    }
    InstallWindows(){
        console.log('Install Windows Complete')
    }
}
exports.Programmer = Programmer;
